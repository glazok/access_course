import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  dark: true, // it's decide your project
  themes: {
    light: {
      prime: '#df8421'
    },
    dark: {
      prime: '#333'
    }
  },
  icons: {
    iconfont: 'mdiSvg',
  },
});
