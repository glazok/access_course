export default {
  props: [
    'form',
    'save',
  ],
  data() {
    return {
      error: null,
      valid: false,
      complete: null,
      loading: false,
      requiredRules: [v => !!v],
    };
  },
  methods: {
    async _save() {
      if (this.$refs.form.validate()) {
        this.error = null;
        this.loading = true;
        try {
          this.complete = await this.save(this.form);
        } catch (e) {
          this.error = e;
        }
        this.loading = false;
      }
    },
    pickFile() {
      this.$refs.file[0].click();
    },
    onFilePicked(e, fieldId) {
      const files = e.target.files;
      if (files[0] !== undefined) {
        const field = this.form.fields.find(f => f.id === fieldId);
        field.name = files[0].name;
        const fr = new FileReader();
        fr.readAsArrayBuffer(files[0]);
        fr.addEventListener('load', () => {
          field.value = files[0].name;
          field.file = Object.values(new Uint8Array(fr.result));
        });
      }
    },
  }
};
