import RequestForm from '../RequestForm';

export default {
    components: {
        RequestForm,
    },
    props: ['text', 'large', 'xlarge'],
    data: () => ({
        dialog: false,
    }),
}
