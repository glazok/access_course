import {saveRequest} from "@/api/rest";

export default {
    data: () => ({
        sent: false,
        valid: true,
        name: '',
        phone: '',
        checkbox: false,
    }),

    methods: {
        async save () {
            if (this.$refs.form.validate()) {
                try {
                    await saveRequest({
                        phone: this.phone,
                        name: this.name,
                    });
                    this.sent = true;
                } catch (e) {
                    console.error(e);
                    alert('Запрос отклонён. Возможно вы уже отправляли заявку?');
                }
            }
        },
    },
}