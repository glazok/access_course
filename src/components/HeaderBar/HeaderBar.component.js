import {getCourses} from "@/api/rest";
import RequestButton from '../RequestButton';
import {groupBy} from 'lodash';

export default {
  components: {
    RequestButton,
  },
  data: () => ({
    fav: true,
    menu: false,
    message: false,
    hints: true,
    types: [],
  }),
  async mounted() {
    const titles = {
      'ege': 'Курсы ЕГЭ',
      'oge': 'Курсы ОГЭ',
      'additional': 'Дополнительно',
    };
    const courses = await getCourses();
    const coursesByType = groupBy(courses, 'type');
    this.types = Object.keys(coursesByType).map(c => ({
      title: titles[c],
      courses: coursesByType[c],
    }));
  }
}
