import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './pages/Home';
import Contacts from './pages/Contacts';
import Course from './pages/Course';

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/contacts',
      component: Contacts
    },
    {
      path: '/course/:id',
      props: true,
      component: Course
    },
  ]
})
