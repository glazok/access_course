import School2019 from './Events/School2019.event';
import Nonexistent from './Events/Nonexistent.event';

export default {
  props: [
    'event_id'
  ],
  components: {
    School2019,
    Nonexistent,
  },
  data() {
    return {
      events: {
        school2019: {
          component: School2019
        }
      }
    };
  },
  computed: {
    currentView() {
      const event = this.events[this.event_id];
      if (event) {
        return event.component;
      }
      return Nonexistent;
    }
  }
}