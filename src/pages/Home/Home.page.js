import ArticleCard from '../../components/ArticleCard';
import EventCard from '../../components/EventCard';
import PrimaryBlock from '../../components/PrimaryBlock';
import StatsBlock from '../../components/StatsBlock';
import TrainingProcessBlock from '../../components/TrainingProcessBlock';
import SliderBlock from '../../components/SliderBlock';
import FeedbackBlock from '../../components/FeedbackBlock';
import PriceBlock from '../../components/PriceBlock';
import MapBlock from '../../components/MapBlock';

export default {
  components: {
    ArticleCard,
    EventCard,
    PrimaryBlock,
    StatsBlock,
    TrainingProcessBlock,
    SliderBlock,
    FeedbackBlock,
    PriceBlock,
    MapBlock,
  },
};
