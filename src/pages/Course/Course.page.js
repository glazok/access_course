import PrimaryBlock from '../../components/PrimaryBlock';
import SuccessBlock from '../../components/SuccessBlock';
import StatsBlock from '../../components/StatsBlock';
import TrainingProcessBlock from '../../components/TrainingProcessBlock';
import VideoBlock from '../../components/VideoBlock';
import SliderBlock from '../../components/SliderBlock';
import FeedbackBlock from '../../components/FeedbackBlock';
import PriceBlock from '../../components/PriceBlock';
import MapBlock from '../../components/MapBlock';
import {getCourse} from "@/api/rest";

export default {
  components: {
    PrimaryBlock,
    SuccessBlock,
    StatsBlock,
    TrainingProcessBlock,
    VideoBlock,
    SliderBlock,
    FeedbackBlock,
    PriceBlock,
    MapBlock,
  },
  props:['id'],
  data() {
    return {
      course: null,
    };
  },
  watch: {
    async id(id) {
      await this.loadPage(id);
    }
  },
  async mounted() {
    await this.loadPage(this.id);
  },
  methods: {
    async loadPage(id) {
      this.course = await getCourse(id);
    }
  }
};
