
let AppContext = (function () {
  let instance = {
    showSnackbar: null,
    infoNotification(msg) {
      // eslint-disable-next-line
      console.log(msg);
      this.showSnackbar(msg, 'info')
    },
    errorNotification(msg, err) {
      // eslint-disable-next-line
      console.error('ERROR: ' + msg);
      // eslint-disable-next-line
      console.error(err);
      this.showSnackbar(msg, 'error')
    },
    client() {
      return new Promise(res => {
        const i = setInterval(() => {
          if (this.__client) {
            res(this.__client);
            clearInterval(i);
          }
        }, 500);
      });
    }
  };
  return function Construct_singletone () {
    if (instance) {
      return instance;
    }
    if (this && this.constructor === Construct_singletone) {
      instance = this;
    } else {
      return new Construct_singletone();
    }
  };
}());
export default AppContext
