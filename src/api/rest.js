import {API_URL} from "@/api/constants";

async function get(path) {
    const resp = await fetch(`${API_URL}/items/${path}?fields=*,*.*`);
    const {data} = await resp.json();
    return data;
}

async function post(path, props) {
    const resp = await fetch(`${API_URL}/items/${path}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(props),
    });
    const {data, error} = await resp.json();

    if (error) {
        throw new Error(error);
    }

    return data;
}

export const getCourses = async () => get('course');
export const getCourse = async (id) => get(`course/${id ?? ''}`);
export const saveRequest = async (props) => post('request', props);